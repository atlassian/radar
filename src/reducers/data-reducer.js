import _ from 'lodash'

import { DATA_LOAD } from '../action-creators/data-action-creators'

export function filterPaths(paths, query) {
    const queryParts = _.compact(query.split(' '))
    return _.omitBy(paths, (value, key) => queryParts.some((part) => key.indexOf(part) === -1))
}

export function filterPathsByTag(paths, query) {
    return _.pickBy(paths, path =>
        Object.keys(path)
            .some(method => !!path[method].tags && path[method].tags.indexOf(query) !== -1)
    )
}

export function filterPathsByScope(paths, query) {
    return _.pickBy(paths, path =>
        Object.keys(path)
            .some(method => {
                if (!!path[method].security) {
                    const scopes = _.find(path[method].security, o => !!o.oauth2)
                    return !!scopes && scopes.oauth2.indexOf(query) !== -1
                }
                return false
            })
    )
}

export function splitPath(selectedPath) {
    const sections = []
    selectedPath.split('/').forEach(section => {
        sections.push({
            key: section,
            name: section,
            path: `${sections.map(s => s.key).join('/')}/${section}`
        })
    })
    return sections
}

export function getResourcesForPath(path, paths) {
    return {
        resource: paths[path],
        subResources: _.omitBy(paths, (value, key) => key.indexOf(`${path}/`) !== 0)
    }
}

export function reducer(state = null, action) {
    switch (action.type) {
    case DATA_LOAD:
        return action.data
    default:
        return state
    }
}
