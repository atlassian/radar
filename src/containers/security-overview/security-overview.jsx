import './security-overview.less'

import React from 'react'
import { connect } from 'react-redux'

import Page from '../../components/aui/page/page.jsx'
import SecurityDefinitions from '../../components/security-definitions/security-definitions.jsx'

const SecurityOverview = ({ data }) => (<Page>
    <h2 className="security-title">Security</h2>
    <SecurityDefinitions definitions={data.securityDefinitions} />
</Page>)

SecurityOverview.propTypes = {
    data: React.PropTypes.shape({
        securityDefinitions: React.PropTypes.object.isRequired
    })
}

export default connect(s => s, {})(SecurityOverview)
