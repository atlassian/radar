import './page.less'

import React from 'react'

const Page = ({ children, nav }) => {
    let navigation = null
    if (!!nav) {
        navigation = (<div className="aui-page-panel-nav">{nav}</div>)
    }

    return (<div className="aui-page-panel">
        <div className="aui-page-panel-inner">
            {navigation}
            <section className="aui-page-panel-content">
                {children}
            </section>
        </div>
    </div>)
}

Page.propTypes = {
    children: React.PropTypes.any,
    nav: React.PropTypes.element
}

export default Page

